//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Aug  6 14:54:47 2017 by ROOT version 5.34/36
// from TTree tree/tree
// found on file: ring_analysis.root
//////////////////////////////////////////////////////////

#ifndef correlations_h
#define correlations_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class correlations {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Float_t         Qx_ring;
   Float_t         Qy_ring;
   Float_t         Qx_rec;
   Float_t         Qy_rec;
   Float_t         Qx_tpc;
   Float_t         Qy_tpc;
   Float_t         Qx_rtpc;
   Float_t         Qy_rtpc;
   Int_t           tracks;

   // List of branches
   TBranch        *b_Qx_ring;   //!
   TBranch        *b_Qy_ring;   //!
   TBranch        *b_Qx_rec;   //!
   TBranch        *b_Qy_rec;   //!
   TBranch        *b_Qx_tpc;   //!
   TBranch        *b_Qy_tpc;   //!
   TBranch        *b_Qx_rtpc;   //!
   TBranch        *b_Qy_rtpc;   //!
   TBranch        *b_tracks;   //!

   correlations(TTree *tree=0);
   virtual ~correlations();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef correlations_cxx
correlations::correlations(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("ring_analysis.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("ring_analysis.root");
      }
      f->GetObject("tree",tree);

   }
   Init(tree);
}

correlations::~correlations()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t correlations::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t correlations::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void correlations::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Qx_ring", &Qx_ring, &b_Qx_ring);
   fChain->SetBranchAddress("Qy_ring", &Qy_ring, &b_Qy_ring);
   fChain->SetBranchAddress("Qx_rec", &Qx_rec, &b_Qx_rec);
   fChain->SetBranchAddress("Qy_rec", &Qy_rec, &b_Qy_rec);
   fChain->SetBranchAddress("Qx_tpc", &Qx_tpc, &b_Qx_tpc);
   fChain->SetBranchAddress("Qy_tpc", &Qy_tpc, &b_Qy_tpc);
   fChain->SetBranchAddress("Qx_rtpc", &Qx_rtpc, &b_Qx_rtpc);
   fChain->SetBranchAddress("Qy_rtpc", &Qy_rtpc, &b_Qy_rtpc);
   fChain->SetBranchAddress("tracks", &tracks, &b_tracks);
   Notify();
}

Bool_t correlations::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void correlations::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t correlations::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef correlations_cxx
