//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Aug  4 11:07:45 2017 by ROOT version 5.34/36
// from TTree data/data
// found on file: t49run3133.0tree.root
//////////////////////////////////////////////////////////

#ifndef Ring_analysis_h
#define Ring_analysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>  
#include <string>
#include <vector>

using namespace std; 

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class Ring_analysis {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Float_t         fVertex_fPchi2;
   Float_t         fVeto_fAdcHadron[4];
   Int_t           fNRun;
   Int_t           fNEvent;
   Int_t           fTriggerMask;
   Int_t           fDate;
   Int_t           fTime;
   Float_t         fEveto;
   Float_t         fVertexX;
   Float_t         fVertexY;
   Float_t         fVertexZ;
   Int_t           fWfaNbeam;
   Int_t           fWfaNinter;
   Int_t           fWfaBeamTime;
   Int_t           fWfaInterTime;
   Int_t           fNPrimaryParticles;
   Char_t          fPrimaryParticles_fIdDet[779];   //[fNPrimaryParticles]
   Char_t          fPrimaryParticles_fCharge[779];   //[fNPrimaryParticles]
   UChar_t         fPrimaryParticles_fNPoint[779][4];   //[fNPrimaryParticles]
   UChar_t         fPrimaryParticles_fNFitPoint[779][4];   //[fNPrimaryParticles]
   UChar_t         fPrimaryParticles_fNDedxPoint[779][4];   //[fNPrimaryParticles]
   UChar_t         fPrimaryParticles_fNMaxPoint[779][4];   //[fNPrimaryParticles]
   UShort_t        fPrimaryParticles_fTmeanCharge[779][4];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fPz[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fEta[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fPhi[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fPt[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fSigPx[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fSigPy[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fBx[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fBy[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fPchi2[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fXFirst[779][4];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fYFirst[779][4];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fZFirst[779][4];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fXLast[779][4];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fYLast[779][4];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fZLast[779][4];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fLabel [779];   //[fNPrimaryParticles]
   Int_t           fPrimaryParticles_fTofIflag[779];   //[fNPrimaryParticles]
   Int_t           fPrimaryParticles_fTofId[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fTofX[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fTofY[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fTofPathl[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fTofCharge[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fTofMass2[779];   //[fNPrimaryParticles]
   Float_t         fPrimaryParticles_fTofSigMass2[779];   //[fNPrimaryParticles]
   Float_t         fRing_fADChadron[240];

   // List of branches
   TBranch        *b_fVertex_fPchi2;   //!
   TBranch        *b_Veto_fAdcHadron;   //!
   TBranch        *b_fNRun;   //!
   TBranch        *b_fNEvent;   //!
   TBranch        *b_fTriggerMask;   //!
   TBranch        *b_fDate;   //!
   TBranch        *b_fTime;   //!
   TBranch        *b_fEveto;   //!
   TBranch        *b_fVertexX;   //!
   TBranch        *b_fVertexY;   //!
   TBranch        *b_fVertexZ;   //!
   TBranch        *b_fWfaNbeam;   //!
   TBranch        *b_fWfaNinter;   //!
   TBranch        *b_fWfaBeamTime;   //!
   TBranch        *b_fWfaInterTime;   //!
   TBranch        *b_fNPrimaryParticles;   //!
   TBranch        *b_fPrimaryParticles_fIdDet;   //!
   TBranch        *b_fPrimaryParticles_fCharge;   //!
   TBranch        *b_fPrimaryParticles_fNPoint;   //!
   TBranch        *b_fPrimaryParticles_fNFitPoint;   //!
   TBranch        *b_fPrimaryParticles_fNDedxPoint;   //!
   TBranch        *b_fPrimaryParticles_fNMaxPoint;   //!
   TBranch        *b_fPrimaryParticles_fTmeanCharge;   //!
   TBranch        *b_fPrimaryParticles_fPz;   //!
   TBranch        *b_fPrimaryParticles_fEta;   //!
   TBranch        *b_fPrimaryParticles_fPhi;   //!
   TBranch        *b_fPrimaryParticles_fPt;   //!
   TBranch        *b_fPrimaryParticles_fSigPx;   //!
   TBranch        *b_fPrimaryParticles_fSigPy;   //!
   TBranch        *b_fPrimaryParticles_fBx;   //!
   TBranch        *b_fPrimaryParticles_fBy;   //!
   TBranch        *b_fPrimaryParticles_fPchi2;   //!
   TBranch        *b_fPrimaryParticles_fXFirst;   //!
   TBranch        *b_fPrimaryParticles_fYFirst;   //!
   TBranch        *b_fPrimaryParticles_fZFirst;   //!
   TBranch        *b_fPrimaryParticles_fXLast;   //!
   TBranch        *b_fPrimaryParticles_fYLast;   //!
   TBranch        *b_fPrimaryParticles_fZLast;   //!
   TBranch        *b_fPrimaryParticles_fLabel ;   //!
   TBranch        *b_fPrimaryParticles_fTofIflag;   //!
   TBranch        *b_fPrimaryParticles_fTofId;   //!
   TBranch        *b_fPrimaryParticles_fTofX;   //!
   TBranch        *b_fPrimaryParticles_fTofY;   //!
   TBranch        *b_fPrimaryParticles_fTofPathl;   //!
   TBranch        *b_fPrimaryParticles_fTofCharge;   //!
   TBranch        *b_fPrimaryParticles_fTofMass2;   //!
   TBranch        *b_fPrimaryParticles_fTofSigMass2;   //!
   TBranch        *b_fRing_fADChadron;   //!

   Ring_analysis(TTree *tree=0);
   virtual ~Ring_analysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   virtual Bool_t  choose_cut(int number_cut);
};

#endif

#ifdef Ring_analysis_cxx
Ring_analysis::Ring_analysis(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      cout<<"There is no tree";
   }
   Init(tree);
}

Ring_analysis::~Ring_analysis()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Ring_analysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Ring_analysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Ring_analysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("fVertex_fPchi2", &fVertex_fPchi2, &b_fVertex_fPchi2);
   fChain->SetBranchAddress("fVeto_fAdcHadron", fVeto_fAdcHadron, &b_Veto_fAdcHadron);
   fChain->SetBranchAddress("fNRun", &fNRun, &b_fNRun);
   fChain->SetBranchAddress("fNEvent", &fNEvent, &b_fNEvent);
   fChain->SetBranchAddress("fTriggerMask", &fTriggerMask, &b_fTriggerMask);
   fChain->SetBranchAddress("fDate", &fDate, &b_fDate);
   fChain->SetBranchAddress("fTime", &fTime, &b_fTime);
   fChain->SetBranchAddress("fEveto", &fEveto, &b_fEveto);
   fChain->SetBranchAddress("fVertexX", &fVertexX, &b_fVertexX);
   fChain->SetBranchAddress("fVertexY", &fVertexY, &b_fVertexY);
   fChain->SetBranchAddress("fVertexZ", &fVertexZ, &b_fVertexZ);
   fChain->SetBranchAddress("fWfaNbeam", &fWfaNbeam, &b_fWfaNbeam);
   fChain->SetBranchAddress("fWfaNinter", &fWfaNinter, &b_fWfaNinter);
   fChain->SetBranchAddress("fWfaBeamTime", &fWfaBeamTime, &b_fWfaBeamTime);
   fChain->SetBranchAddress("fWfaInterTime", &fWfaInterTime, &b_fWfaInterTime);
   fChain->SetBranchAddress("fNPrimaryParticles", &fNPrimaryParticles, &b_fNPrimaryParticles);
   fChain->SetBranchAddress("fPrimaryParticles_fIdDet", fPrimaryParticles_fIdDet, &b_fPrimaryParticles_fIdDet);
   fChain->SetBranchAddress("fPrimaryParticles_fCharge", fPrimaryParticles_fCharge, &b_fPrimaryParticles_fCharge);
   fChain->SetBranchAddress("fPrimaryParticles_fNPoint", fPrimaryParticles_fNPoint, &b_fPrimaryParticles_fNPoint);
   fChain->SetBranchAddress("fPrimaryParticles_fNFitPoint", fPrimaryParticles_fNFitPoint, &b_fPrimaryParticles_fNFitPoint);
   fChain->SetBranchAddress("fPrimaryParticles_fNDedxPoint", fPrimaryParticles_fNDedxPoint, &b_fPrimaryParticles_fNDedxPoint);
   fChain->SetBranchAddress("fPrimaryParticles_fNMaxPoint", fPrimaryParticles_fNMaxPoint, &b_fPrimaryParticles_fNMaxPoint);
   fChain->SetBranchAddress("fPrimaryParticles_fTmeanCharge", fPrimaryParticles_fTmeanCharge, &b_fPrimaryParticles_fTmeanCharge);
   fChain->SetBranchAddress("fPrimaryParticles_fPz", fPrimaryParticles_fPz, &b_fPrimaryParticles_fPz);
   fChain->SetBranchAddress("fPrimaryParticles_fEta", fPrimaryParticles_fEta, &b_fPrimaryParticles_fEta);
   fChain->SetBranchAddress("fPrimaryParticles_fPhi", fPrimaryParticles_fPhi, &b_fPrimaryParticles_fPhi);
   fChain->SetBranchAddress("fPrimaryParticles_fPt", fPrimaryParticles_fPt, &b_fPrimaryParticles_fPt);
   fChain->SetBranchAddress("fPrimaryParticles_fSigPx", fPrimaryParticles_fSigPx, &b_fPrimaryParticles_fSigPx);
   fChain->SetBranchAddress("fPrimaryParticles_fSigPy", fPrimaryParticles_fSigPy, &b_fPrimaryParticles_fSigPy);
   fChain->SetBranchAddress("fPrimaryParticles_fBx", fPrimaryParticles_fBx, &b_fPrimaryParticles_fBx);
   fChain->SetBranchAddress("fPrimaryParticles_fBy", fPrimaryParticles_fBy, &b_fPrimaryParticles_fBy);
   fChain->SetBranchAddress("fPrimaryParticles_fPchi2", fPrimaryParticles_fPchi2, &b_fPrimaryParticles_fPchi2);
   fChain->SetBranchAddress("fPrimaryParticles_fXFirst", fPrimaryParticles_fXFirst, &b_fPrimaryParticles_fXFirst);
   fChain->SetBranchAddress("fPrimaryParticles_fYFirst", fPrimaryParticles_fYFirst, &b_fPrimaryParticles_fYFirst);
   fChain->SetBranchAddress("fPrimaryParticles_fZFirst", fPrimaryParticles_fZFirst, &b_fPrimaryParticles_fZFirst);
   fChain->SetBranchAddress("fPrimaryParticles_fXLast", fPrimaryParticles_fXLast, &b_fPrimaryParticles_fXLast);
   fChain->SetBranchAddress("fPrimaryParticles_fYLast", fPrimaryParticles_fYLast, &b_fPrimaryParticles_fYLast);
   fChain->SetBranchAddress("fPrimaryParticles_fZLast", fPrimaryParticles_fZLast, &b_fPrimaryParticles_fZLast);
   fChain->SetBranchAddress("fPrimaryParticles_fLabel ", fPrimaryParticles_fLabel , &b_fPrimaryParticles_fLabel );
   fChain->SetBranchAddress("fPrimaryParticles_fTofIflag", fPrimaryParticles_fTofIflag, &b_fPrimaryParticles_fTofIflag);
   fChain->SetBranchAddress("fPrimaryParticles_fTofId", fPrimaryParticles_fTofId, &b_fPrimaryParticles_fTofId);
   fChain->SetBranchAddress("fPrimaryParticles_fTofX", fPrimaryParticles_fTofX, &b_fPrimaryParticles_fTofX);
   fChain->SetBranchAddress("fPrimaryParticles_fTofY", fPrimaryParticles_fTofY, &b_fPrimaryParticles_fTofY);
   fChain->SetBranchAddress("fPrimaryParticles_fTofPathl", fPrimaryParticles_fTofPathl, &b_fPrimaryParticles_fTofPathl);
   fChain->SetBranchAddress("fPrimaryParticles_fTofCharge", fPrimaryParticles_fTofCharge, &b_fPrimaryParticles_fTofCharge);
   fChain->SetBranchAddress("fPrimaryParticles_fTofMass2", fPrimaryParticles_fTofMass2, &b_fPrimaryParticles_fTofMass2);
   fChain->SetBranchAddress("fPrimaryParticles_fTofSigMass2", fPrimaryParticles_fTofSigMass2, &b_fPrimaryParticles_fTofSigMass2);
   fChain->SetBranchAddress("fRing_fADChadron", fRing_fADChadron, &b_fRing_fADChadron);
   Notify();
}

Bool_t Ring_analysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Ring_analysis::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Ring_analysis::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Ring_analysis_cxx
